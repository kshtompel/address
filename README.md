# Not implemented features
I did not understand what did you mean about url /api.validate-address-by-id so i did not implement it

# Install & build

```
cd ./build
docker-composer up --build
```

# Request
```
curl -X POST -d '{"country": "asd", "city": "zxc", "zip":123}' http://127.0.0.1:8000/api/address-validate -H "Content-Type: application/json"
```


# PHP unit

```
./bin/phpunit
```