<?php

namespace App\Infrastructure\Core\EventListener;


use App\Infrastructure\Core\Exception\ViolationListException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ConvertExceptionToDataSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'convertExceptionToData',
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function convertExceptionToData(ExceptionEvent $event): void
    {
        $request = $event->getRequest();

        if ($request->getContentType() !== 'json') {
            return;
        }

        $e = $event->getException();

        $data = ['valid' => false];
        switch (true) {
            case $e instanceof ViolationListException:
                $data['error'] = $e->getErrors();
                break;

            default:
                $data['error'] = $e->getMessage();
        }

        $event->setResponse(new JsonResponse($data));
    }
}