<?php

namespace App\Infrastructure\Core\Exception;


use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ViolationListException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    /**
     * ViolationListException constructor.
     *
     * @param ConstraintViolationListInterface $violations
     * @param string                           $message
     * @param int                              $code
     * @param \Throwable|null                  $previous
     */
    public function __construct(ConstraintViolationListInterface $violations, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        $data = [];

        foreach ($this->violations as $violation) {
            /** @var ConstraintViolationInterface $violation */
            $data[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $data;
    }
}