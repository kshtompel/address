<?php

namespace App\Infrastructure\Address\Controller;

use App\Address\Service\AddressResolverInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ValidateController
{
    /**
     * @var AddressResolverInterface
     */
    private $addressPersister;

    /**
     * ValidateController constructor.
     *
     * @param AddressResolverInterface $addressPersister
     */
    public function __construct(AddressResolverInterface $addressPersister)
    {
        $this->addressPersister = $addressPersister;
    }

    /**
     * @Route("/address-validate")
     * @param Request $request
     */
    public function validateAddress(Request $request)
    {
        $this->addressPersister->resolve($request->request->all());

        return new JsonResponse(['valid' => true]);
    }
}