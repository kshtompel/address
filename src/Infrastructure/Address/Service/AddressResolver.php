<?php

namespace App\Infrastructure\Address\Service;


use App\Address\Model\Address;
use App\Address\Service\AddressResolverInterface;
use App\Infrastructure\Core\Exception\ViolationListException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddressResolver implements AddressResolverInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var OptionsResolver
     */
    private $optionResolver;

    /**
     * AddressPersister constructor.
     *
     * @param ValidatorInterface     $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->optionResolver = $this->initOptions();
    }

    /**
     * @param array $addressData
     */
    public function resolve(array $addressData): void
    {
        $data = $this->optionResolver->resolve($addressData);

        $address = new Address($data['country'], $data['city'], $data['zip']);

        $violations = $this->validator->validate($address);

        if ($violations->count() > 0) {
            throw new ViolationListException($violations);
        }
    }

    /**
     * @return OptionsResolver
     */
    private function initOptions()
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'country' => null,
            'city' => null,
            'zip' => null,
        ]);

        return $resolver;
    }
}