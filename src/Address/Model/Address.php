<?php

namespace App\Address\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Address
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var null|string
     *
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * @var null|string
     *
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var null|string
     *
     * @Assert\NotBlank()
     */
    private $zip;

    /**
     * Address constructor.
     *
     * @param null|string $country
     * @param null|string $city
     * @param null|string $zip
     */
    public function __construct(?string $country, ?string $city, ?string $zip)
    {
        $this->country = $country;
        $this->city = $city;
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function zip(): string
    {
        return $this->zip;
    }
}