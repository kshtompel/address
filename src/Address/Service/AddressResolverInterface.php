<?php

namespace App\Address\Service;


interface AddressResolverInterface
{
    /**
     * @param array $addressData
     */
    public function resolve(array $addressData): void;
}