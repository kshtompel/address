<?php

namespace App\Tests\Unit\Infrastructure\Address\Service;

use App\Infrastructure\Address\Service\AddressResolver;
use App\Infrastructure\Core\Exception\ViolationListException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;

class AddressResolverTest extends WebTestCase
{
    public function providerTest(): array
    {
        return [
            [
                [
                    'data' => [],
                    'exception' => ViolationListException::class,
                    'expectedErrors' => [
                        'country' => ['This value should not be blank.'],
                        'city' => ['This value should not be blank.'],
                        'zip' => ['This value should not be blank.'],
                    ]
                ],
            ],
            [
                [
                    'data' => [
                        'asd' => 'asd'
                    ],
                    'exception' => UndefinedOptionsException::class,
                    'expectedErrors' => 'The option "asd" does not exist. Defined options are: "city", "country", "zip".'
                ],
            ],
            [
                [
                    'data' => [
                        'country' => 'Ukraine'
                    ],
                    'exception' => ViolationListException::class,
                    'expectedErrors' => [
                        'city' => ['This value should not be blank.'],
                        'zip' => ['This value should not be blank.'],
                    ]
                ],
            ],
            [
                [
                    'data' => [
                        'city' => 'Kyiv'
                    ],
                    'exception' => ViolationListException::class,
                    'expectedErrors' => [
                        'country' => ['This value should not be blank.'],
                        'zip' => ['This value should not be blank.'],
                    ]
                ],
            ],
            [
                [
                    'data' => [
                        'zip' => 123,
                    ],
                    'exception' => ViolationListException::class,
                    'expectedErrors' => [
                        'country' => ['This value should not be blank.'],
                        'city' => ['This value should not be blank.'],
                    ]
                ],
            ],
            [
                [
                    'data' => [
                        'country' => 'Ukraine',
                        'city' => 'Kyiv',
                        'zip' => 5123,
                    ],
                    'exception' => null,
                    'expectedErrors' => []
                ]
            ]
        ];
    }

    /**
     * @dataProvider providerTest
     */
    public function test($testCase)
    {
        $subject = static::$kernel->getContainer()->get(AddressResolver::class);

        try {
            $subject->resolve($testCase['data']);
        } catch (ViolationListException $e) {
            $this->assertSame(
                $testCase['expectedErrors'],
                $e->getErrors()
            );
            throw $e;
        } catch (\Exception $e) {
            $this->assertSame(
                $testCase['expectedErrors'],
                $e->getMessage()
            );
            throw $e;
        }
    }

    /**
     *
     */
    protected function setUp()
    {
        self::bootKernel();
        parent::setUp();
    }

    private function getMock()
    {
        $emMock = $this->getMockForAbstractClass(EntityManagerInterface::class);

        $emMock->expects($this->exactly(null === $testCase['exception'] ? 1 : 0))->method('persist');
        $emMock->expects($this->exactly(null === $testCase['exception'] ? 1 : 0))->method('flush');

        if (null !== $testCase['exception']) {
            $this->expectException($testCase['exception']);
        }

        return $emMock;
    }
}